<sld_project_info>
  <sld_infos>
    <sld_info hpath="bup_qsys_inst|tse_0_tse" library="tse_mac" name="tse_0_tse">
      <assignment_values>
        <assignment_value text="QSYS_NAME tse_mac HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|tse_0_dma_tx" library="bup_qsys_msgdma_tx" name="tse_0_dma_tx">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_msgdma_tx HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|tse_0_dma_rx" library="bup_qsys_msgdma_rx" name="tse_0_dma_rx">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_msgdma_rx HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|sysid" library="bup_qsys_sysid" name="sysid">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_sysid HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|sys_clk_timer" library="bup_qsys_sys_clk_timer" name="sys_clk_timer">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_sys_clk_timer HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|sys_clk" library="bup_qsys_sys_clk" name="sys_clk">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_sys_clk HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|opencores_i2c_0" library="bup_qsys_opencores_i2c_0" name="opencores_i2c_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_opencores_i2c_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|onchip_ram_m9" library="bup_qsys_onchip_ram_m9" name="onchip_ram_m9">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_onchip_ram_m9 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|led_pio" library="bup_qsys_led_pio" name="led_pio">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_led_pio HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|jtag_uart" library="bup_qsys_jtag_uart" name="jtag_uart">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_jtag_uart HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|flash_select" library="bup_qsys_pio_1" name="flash_select">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_pio_1 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|ext_flash" library="bup_qsys_generic_tristate_controller_0" name="ext_flash">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_generic_tristate_controller_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|descriptor_memory" library="descriptor_memory" name="descriptor_memory">
      <assignment_values>
        <assignment_value text="QSYS_NAME descriptor_memory HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|cpu" library="bup_qsys_nios2_gen2_0" name="cpu">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_nios2_gen2_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|clk_125m" library="clk_125m" name="clk_125m">
      <assignment_values>
        <assignment_value text="QSYS_NAME clk_125m HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|button_pio" library="bup_qsys_pio_0" name="button_pio">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_pio_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|bup_qsys_cfi_flash_atb_bridge_0" library="bup_qsys_cfi_flash_atb_bridge_0" name="bup_qsys_cfi_flash_atb_bridge_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys_cfi_flash_atb_bridge_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst" library="bup_qsys" name="bup_qsys_inst">
      <assignment_values>
        <assignment_value text="QSYS_NAME bup_qsys HAS_SOPCINFO 1 GENERATION_ID 0"/>
        <assignment_value text="QSYS_NAME bup_qsys HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="auto_fab_0" name="auto_fab_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab_0 HAS_SOPCINFO 1 GENERATION_ID 0 ENTITY_NAME alt_sld_fab SLD_FAB 1 DESIGN_HASH 61cd1ec1369d1744384d"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|opencores_i2c_0|bup_qsys_opencores_i2c_0|gpio_sda" library="io_opendrain" name="gpio_sda">
      <assignment_values>
        <assignment_value text="QSYS_NAME io_opendrain HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="bup_qsys_inst|opencores_i2c_0|bup_qsys_opencores_i2c_0|gpio_scl" library="io_opendrain" name="gpio_scl">
      <assignment_values>
        <assignment_value text="QSYS_NAME io_opendrain HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
