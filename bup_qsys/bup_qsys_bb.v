module bup_qsys (
		output wire [26:0] bup_qsys_cfi_flash_atb_bridge_0_out_tcm_address_out,      // bup_qsys_cfi_flash_atb_bridge_0_out.tcm_address_out
		output wire [0:0]  bup_qsys_cfi_flash_atb_bridge_0_out_tcm_read_n_out,       //                                    .tcm_read_n_out
		output wire [0:0]  bup_qsys_cfi_flash_atb_bridge_0_out_tcm_write_n_out,      //                                    .tcm_write_n_out
		inout  wire [15:0] bup_qsys_cfi_flash_atb_bridge_0_out_tcm_data_out,         //                                    .tcm_data_out
		output wire [0:0]  bup_qsys_cfi_flash_atb_bridge_0_out_tcm_chipselect_n_out, //                                    .tcm_chipselect_n_out
		input  wire [2:0]  button_pio_external_connection_export,                    //      button_pio_external_connection.export
		input  wire        clk_125m_clk,                                             //                            clk_125m.clk
		output wire        flash_select_external_connection_export,                  //    flash_select_external_connection.export
		output wire [7:0]  led_pio_out_export,                                       //                         led_pio_out.export
		inout  wire        opencores_i2c_export_0_scl_pad_io,                        //              opencores_i2c_export_0.scl_pad_io
		inout  wire        opencores_i2c_export_0_sda_pad_io,                        //                                    .sda_pad_io
		input  wire        reset_1_reset_n,                                          //                             reset_1.reset_n
		input  wire        reset_125m_reset_n,                                       //                          reset_125m.reset_n
		input  wire        sys_clk_clk,                                              //                             sys_clk.clk
		output wire        tse_mac_mac_mdio_connection_mdc,                          //         tse_mac_mac_mdio_connection.mdc
		input  wire        tse_mac_mac_mdio_connection_mdio_in,                      //                                    .mdio_in
		output wire        tse_mac_mac_mdio_connection_mdio_out,                     //                                    .mdio_out
		output wire        tse_mac_mac_mdio_connection_mdio_oen,                     //                                    .mdio_oen
		output wire        tse_mac_misc_connection_magic_wakeup,                     //             tse_mac_misc_connection.magic_wakeup
		input  wire        tse_mac_misc_connection_magic_sleep_n,                    //                                    .magic_sleep_n
		input  wire        tse_mac_misc_connection_ff_tx_crc_fwd,                    //                                    .ff_tx_crc_fwd
		output wire        tse_mac_misc_connection_ff_tx_septy,                      //                                    .ff_tx_septy
		output wire        tse_mac_misc_connection_tx_ff_uflow,                      //                                    .tx_ff_uflow
		output wire        tse_mac_misc_connection_ff_tx_a_full,                     //                                    .ff_tx_a_full
		output wire        tse_mac_misc_connection_ff_tx_a_empty,                    //                                    .ff_tx_a_empty
		output wire [17:0] tse_mac_misc_connection_rx_err_stat,                      //                                    .rx_err_stat
		output wire [3:0]  tse_mac_misc_connection_rx_frm_type,                      //                                    .rx_frm_type
		output wire        tse_mac_misc_connection_ff_rx_dsav,                       //                                    .ff_rx_dsav
		output wire        tse_mac_misc_connection_ff_rx_a_full,                     //                                    .ff_rx_a_full
		output wire        tse_mac_misc_connection_ff_rx_a_empty,                    //                                    .ff_rx_a_empty
		input  wire        tse_mac_serial_connection_rxp_0,                          //           tse_mac_serial_connection.rxp_0
		output wire        tse_mac_serial_connection_txp_0,                          //                                    .txp_0
		output wire        tse_mac_status_led_connection_crs,                        //       tse_mac_status_led_connection.crs
		output wire        tse_mac_status_led_connection_link,                       //                                    .link
		output wire        tse_mac_status_led_connection_panel_link,                 //                                    .panel_link
		output wire        tse_mac_status_led_connection_col,                        //                                    .col
		output wire        tse_mac_status_led_connection_an,                         //                                    .an
		output wire        tse_mac_status_led_connection_char_err,                   //                                    .char_err
		output wire        tse_mac_status_led_connection_disp_err                    //                                    .disp_err
	);
endmodule

