# Stratix 10 Ethernet communication

## Description

This project sets up Stratix 10 FPGA L-Tile (1SG280LU2F50E2VGS3) to an ethernet connection. Data is stored on the on-chip memory of the FPGA and is then retrieved and sent via the ethernet upon request. 


This project uses the [Startix 10 L-Tile Dev kit](https://www.intel.com/content/www/us/en/programmable/products/boards_and_kits/dev-kits/altera/kit-s10-fpga.html).

## Acknowledgements
A huge part of this project is based on work done by [Dr. Sam de Jong](https://github.com/samdejong86/Arria-V-ADC-Ethernet) and the Board Update Portal for the Stratix V Dev Kit. Modifications include the software branch compiled by the NIOS II processor. 

___
This project is still a Work in Progress
