	component io_buffer is
		port (
			din    : in    std_logic_vector(0 downto 0) := (others => 'X'); -- export
			dout   : out   std_logic_vector(0 downto 0);                    -- export
			oe     : in    std_logic_vector(0 downto 0) := (others => 'X'); -- export
			pad_io : inout std_logic_vector(0 downto 0) := (others => 'X')  -- export
		);
	end component io_buffer;

	u0 : component io_buffer
		port map (
			din    => CONNECTED_TO_din,    --    din.export
			dout   => CONNECTED_TO_dout,   --   dout.export
			oe     => CONNECTED_TO_oe,     --     oe.export
			pad_io => CONNECTED_TO_pad_io  -- pad_io.export
		);

