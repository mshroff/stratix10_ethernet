module io_buffer (
		input  wire [0:0] din,    //    din.export
		output wire [0:0] dout,   //   dout.export
		input  wire [0:0] oe,     //     oe.export
		inout  wire [0:0] pad_io  // pad_io.export
	);
endmodule

