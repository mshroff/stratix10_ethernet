	bup_qsys_cfi_flash_atb_bridge_0 u0 (
		.clk                      (_connected_to_clk_),                      //   input,   width = 1,   clk.clk
		.tcm_address_out          (_connected_to_tcm_address_out_),          //  output,  width = 27,   out.tcm_address_out
		.tcm_read_n_out           (_connected_to_tcm_read_n_out_),           //  output,   width = 1,      .tcm_read_n_out
		.tcm_write_n_out          (_connected_to_tcm_write_n_out_),          //  output,   width = 1,      .tcm_write_n_out
		.tcm_data_out             (_connected_to_tcm_data_out_),             //   inout,  width = 16,      .tcm_data_out
		.tcm_chipselect_n_out     (_connected_to_tcm_chipselect_n_out_),     //  output,   width = 1,      .tcm_chipselect_n_out
		.reset                    (_connected_to_reset_),                    //   input,   width = 1, reset.reset
		.request                  (_connected_to_request_),                  //   input,   width = 1,   tcs.request
		.grant                    (_connected_to_grant_),                    //  output,   width = 1,      .grant
		.tcs_tcm_address_out      (_connected_to_tcs_tcm_address_out_),      //   input,  width = 27,      .address_out
		.tcs_tcm_read_n_out       (_connected_to_tcs_tcm_read_n_out_),       //   input,   width = 1,      .read_n_out
		.tcs_tcm_write_n_out      (_connected_to_tcs_tcm_write_n_out_),      //   input,   width = 1,      .write_n_out
		.tcs_tcm_data_out         (_connected_to_tcs_tcm_data_out_),         //   input,  width = 16,      .data_out
		.tcs_tcm_data_outen       (_connected_to_tcs_tcm_data_outen_),       //   input,   width = 1,      .data_outen
		.tcs_tcm_data_in          (_connected_to_tcs_tcm_data_in_),          //  output,  width = 16,      .data_in
		.tcs_tcm_chipselect_n_out (_connected_to_tcs_tcm_chipselect_n_out_)  //   input,   width = 1,      .chipselect_n_out
	);

