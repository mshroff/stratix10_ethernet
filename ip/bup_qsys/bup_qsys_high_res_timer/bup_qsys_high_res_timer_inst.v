	bup_qsys_high_res_timer u0 (
		.clk        (_connected_to_clk_),        //   clk.clk
		.irq        (_connected_to_irq_),        //   irq.irq
		.reset_n    (_connected_to_reset_n_),    // reset.reset_n
		.address    (_connected_to_address_),    //    s1.address
		.writedata  (_connected_to_writedata_),  //      .writedata
		.readdata   (_connected_to_readdata_),   //      .readdata
		.chipselect (_connected_to_chipselect_), //      .chipselect
		.write_n    (_connected_to_write_n_)     //      .write_n
	);

