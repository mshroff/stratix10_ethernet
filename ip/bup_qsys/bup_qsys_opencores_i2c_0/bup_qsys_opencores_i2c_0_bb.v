module bup_qsys_opencores_i2c_0 (
		input  wire [2:0] wb_adr_i,   //   avalon_slave_0.address
		input  wire [7:0] wb_dat_i,   //                 .writedata
		output wire [7:0] wb_dat_o,   //                 .readdata
		input  wire       wb_we_i,    //                 .write
		input  wire       wb_stb_i,   //                 .chipselect
		output wire       wb_ack_o,   //                 .waitrequest_n
		input  wire       wb_clk_i,   //            clock.clk
		input  wire       wb_rst_i,   //      clock_reset.reset
		inout  wire       scl_pad_io, //         export_0.scl_pad_io
		inout  wire       sda_pad_io, //                 .sda_pad_io
		output wire       wb_inta_o   // interrupt_sender.irq
	);
endmodule

