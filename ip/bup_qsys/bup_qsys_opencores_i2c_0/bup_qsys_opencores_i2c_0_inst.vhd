	component bup_qsys_opencores_i2c_0 is
		port (
			wb_adr_i   : in    std_logic_vector(2 downto 0) := (others => 'X'); -- address
			wb_dat_i   : in    std_logic_vector(7 downto 0) := (others => 'X'); -- writedata
			wb_dat_o   : out   std_logic_vector(7 downto 0);                    -- readdata
			wb_we_i    : in    std_logic                    := 'X';             -- write
			wb_stb_i   : in    std_logic                    := 'X';             -- chipselect
			wb_ack_o   : out   std_logic;                                       -- waitrequest_n
			wb_clk_i   : in    std_logic                    := 'X';             -- clk
			wb_rst_i   : in    std_logic                    := 'X';             -- reset
			scl_pad_io : inout std_logic                    := 'X';             -- scl_pad_io
			sda_pad_io : inout std_logic                    := 'X';             -- sda_pad_io
			wb_inta_o  : out   std_logic                                        -- irq
		);
	end component bup_qsys_opencores_i2c_0;

	u0 : component bup_qsys_opencores_i2c_0
		port map (
			wb_adr_i   => CONNECTED_TO_wb_adr_i,   --   avalon_slave_0.address
			wb_dat_i   => CONNECTED_TO_wb_dat_i,   --                 .writedata
			wb_dat_o   => CONNECTED_TO_wb_dat_o,   --                 .readdata
			wb_we_i    => CONNECTED_TO_wb_we_i,    --                 .write
			wb_stb_i   => CONNECTED_TO_wb_stb_i,   --                 .chipselect
			wb_ack_o   => CONNECTED_TO_wb_ack_o,   --                 .waitrequest_n
			wb_clk_i   => CONNECTED_TO_wb_clk_i,   --            clock.clk
			wb_rst_i   => CONNECTED_TO_wb_rst_i,   --      clock_reset.reset
			scl_pad_io => CONNECTED_TO_scl_pad_io, --         export_0.scl_pad_io
			sda_pad_io => CONNECTED_TO_sda_pad_io, --                 .sda_pad_io
			wb_inta_o  => CONNECTED_TO_wb_inta_o   -- interrupt_sender.irq
		);

