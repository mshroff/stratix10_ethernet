
module bup_qsys_merged_resets (
	in_reset_n,
	out_reset_n);	

	input		in_reset_n;
	output		out_reset_n;
endmodule
