	component bup_qsys_merlin_slave_translator_0 is
		port (
			av_address        : out std_logic_vector(7 downto 0);                     -- address
			av_write          : out std_logic;                                        -- write
			av_read           : out std_logic;                                        -- read
			av_readdata       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
			av_writedata      : out std_logic_vector(31 downto 0);                    -- writedata
			av_waitrequest    : in  std_logic                     := 'X';             -- waitrequest
			uav_address       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- address
			uav_burstcount    : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- burstcount
			uav_read          : in  std_logic                     := 'X';             -- read
			uav_write         : in  std_logic                     := 'X';             -- write
			uav_waitrequest   : out std_logic;                                        -- waitrequest
			uav_readdatavalid : out std_logic;                                        -- readdatavalid
			uav_byteenable    : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
			uav_readdata      : out std_logic_vector(31 downto 0);                    -- readdata
			uav_writedata     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			uav_lock          : in  std_logic                     := 'X';             -- lock
			uav_debugaccess   : in  std_logic                     := 'X';             -- debugaccess
			clk               : in  std_logic                     := 'X';             -- clk
			reset             : in  std_logic                     := 'X'              -- reset
		);
	end component bup_qsys_merlin_slave_translator_0;

	u0 : component bup_qsys_merlin_slave_translator_0
		port map (
			av_address        => CONNECTED_TO_av_address,        --      avalon_anti_slave_0.address
			av_write          => CONNECTED_TO_av_write,          --                         .write
			av_read           => CONNECTED_TO_av_read,           --                         .read
			av_readdata       => CONNECTED_TO_av_readdata,       --                         .readdata
			av_writedata      => CONNECTED_TO_av_writedata,      --                         .writedata
			av_waitrequest    => CONNECTED_TO_av_waitrequest,    --                         .waitrequest
			uav_address       => CONNECTED_TO_uav_address,       -- avalon_universal_slave_0.address
			uav_burstcount    => CONNECTED_TO_uav_burstcount,    --                         .burstcount
			uav_read          => CONNECTED_TO_uav_read,          --                         .read
			uav_write         => CONNECTED_TO_uav_write,         --                         .write
			uav_waitrequest   => CONNECTED_TO_uav_waitrequest,   --                         .waitrequest
			uav_readdatavalid => CONNECTED_TO_uav_readdatavalid, --                         .readdatavalid
			uav_byteenable    => CONNECTED_TO_uav_byteenable,    --                         .byteenable
			uav_readdata      => CONNECTED_TO_uav_readdata,      --                         .readdata
			uav_writedata     => CONNECTED_TO_uav_writedata,     --                         .writedata
			uav_lock          => CONNECTED_TO_uav_lock,          --                         .lock
			uav_debugaccess   => CONNECTED_TO_uav_debugaccess,   --                         .debugaccess
			clk               => CONNECTED_TO_clk,               --                      clk.clk
			reset             => CONNECTED_TO_reset              --                    reset.reset
		);

