	bup_qsys_clk_125m u0 (
		.clk_out     (_connected_to_clk_out_),     //          clk.clk
		.in_clk      (_connected_to_in_clk_),      //       clk_in.clk
		.reset_n     (_connected_to_reset_n_),     // clk_in_reset.reset_n
		.reset_n_out (_connected_to_reset_n_out_)  //    clk_reset.reset_n
	);

