
module merged_resetn (
	clk,
	in_reset_n,
	out_reset_n);	

	input		clk;
	input		in_reset_n;
	output		out_reset_n;
endmodule
