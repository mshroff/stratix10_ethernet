	bup_qsys_ext_flash u0 (
		.clk_clk              (_connected_to_clk_clk_),              //   clk.clk
		.reset_reset          (_connected_to_reset_reset_),          // reset.reset
		.tcm_write_n_out      (_connected_to_tcm_write_n_out_),      //   tcm.write_n_out
		.tcm_read_n_out       (_connected_to_tcm_read_n_out_),       //      .read_n_out
		.tcm_chipselect_n_out (_connected_to_tcm_chipselect_n_out_), //      .chipselect_n_out
		.tcm_request          (_connected_to_tcm_request_),          //      .request
		.tcm_grant            (_connected_to_tcm_grant_),            //      .grant
		.tcm_address_out      (_connected_to_tcm_address_out_),      //      .address_out
		.tcm_data_out         (_connected_to_tcm_data_out_),         //      .data_out
		.tcm_data_outen       (_connected_to_tcm_data_outen_),       //      .data_outen
		.tcm_data_in          (_connected_to_tcm_data_in_),          //      .data_in
		.uas_address          (_connected_to_uas_address_),          //   uas.address
		.uas_burstcount       (_connected_to_uas_burstcount_),       //      .burstcount
		.uas_read             (_connected_to_uas_read_),             //      .read
		.uas_write            (_connected_to_uas_write_),            //      .write
		.uas_waitrequest      (_connected_to_uas_waitrequest_),      //      .waitrequest
		.uas_readdatavalid    (_connected_to_uas_readdatavalid_),    //      .readdatavalid
		.uas_byteenable       (_connected_to_uas_byteenable_),       //      .byteenable
		.uas_readdata         (_connected_to_uas_readdata_),         //      .readdata
		.uas_writedata        (_connected_to_uas_writedata_),        //      .writedata
		.uas_lock             (_connected_to_uas_lock_),             //      .lock
		.uas_debugaccess      (_connected_to_uas_debugaccess_)       //      .debugaccess
	);

