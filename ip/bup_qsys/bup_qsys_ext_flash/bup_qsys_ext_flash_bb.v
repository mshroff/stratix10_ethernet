
module bup_qsys_ext_flash (
	clk_clk,
	reset_reset,
	tcm_write_n_out,
	tcm_read_n_out,
	tcm_chipselect_n_out,
	tcm_request,
	tcm_grant,
	tcm_address_out,
	tcm_data_out,
	tcm_data_outen,
	tcm_data_in,
	uas_address,
	uas_burstcount,
	uas_read,
	uas_write,
	uas_waitrequest,
	uas_readdatavalid,
	uas_byteenable,
	uas_readdata,
	uas_writedata,
	uas_lock,
	uas_debugaccess);	

	input		clk_clk;
	input		reset_reset;
	output		tcm_write_n_out;
	output		tcm_read_n_out;
	output		tcm_chipselect_n_out;
	output		tcm_request;
	input		tcm_grant;
	output	[27:0]	tcm_address_out;
	output	[31:0]	tcm_data_out;
	output		tcm_data_outen;
	input	[31:0]	tcm_data_in;
	input	[27:0]	uas_address;
	input	[2:0]	uas_burstcount;
	input		uas_read;
	input		uas_write;
	output		uas_waitrequest;
	output		uas_readdatavalid;
	input	[3:0]	uas_byteenable;
	output	[31:0]	uas_readdata;
	input	[31:0]	uas_writedata;
	input		uas_lock;
	input		uas_debugaccess;
endmodule
