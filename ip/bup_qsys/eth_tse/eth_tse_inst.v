	eth_tse u0 (
		.avalon_slave_address         (_connected_to_avalon_slave_address_),         //  avalon_slave_eth.address
		.avalon_slave_write           (_connected_to_avalon_slave_write_),           //                  .write
		.avalon_slave_read            (_connected_to_avalon_slave_read_),            //                  .read
		.avalon_slave_writedata       (_connected_to_avalon_slave_writedata_),       //                  .writedata
		.avalon_slave_readdata        (_connected_to_avalon_slave_readdata_),        //                  .readdata
		.avalon_slave_waitrequest     (_connected_to_avalon_slave_waitrequest_),     //                  .waitrequest
		.avalon_slave_eth_address     (_connected_to_avalon_slave_eth_address_),     // avalon_slave_eth1.address
		.avalon_slave_eth_write       (_connected_to_avalon_slave_eth_write_),       //                  .write
		.avalon_slave_eth_read        (_connected_to_avalon_slave_eth_read_),        //                  .read
		.avalon_slave_eth_writedata   (_connected_to_avalon_slave_eth_writedata_),   //                  .writedata
		.avalon_slave_eth_readdata    (_connected_to_avalon_slave_eth_readdata_),    //                  .readdata
		.avalon_slave_eth_waitrequest (_connected_to_avalon_slave_eth_waitrequest_), //                  .waitrequest
		.clock_sink_clk               (_connected_to_clock_sink_clk_),               //    clock_sink_eth.clk
		.reset_source_reset           (_connected_to_reset_source_reset_),           //    reset_sink_eth.reset
		.reset_sink_eth_reset         (_connected_to_reset_sink_eth_reset_)          //   reset_sink_eth1.reset
	);

