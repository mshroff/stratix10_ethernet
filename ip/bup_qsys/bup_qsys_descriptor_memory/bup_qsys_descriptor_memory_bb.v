
module bup_qsys_descriptor_memory (
	clk,
	reset,
	reset_req,
	address,
	clken,
	chipselect,
	write,
	readdata,
	writedata,
	byteenable);	

	input		clk;
	input		reset;
	input		reset_req;
	input	[10:0]	address;
	input		clken;
	input		chipselect;
	input		write;
	output	[31:0]	readdata;
	input	[31:0]	writedata;
	input	[3:0]	byteenable;
endmodule
