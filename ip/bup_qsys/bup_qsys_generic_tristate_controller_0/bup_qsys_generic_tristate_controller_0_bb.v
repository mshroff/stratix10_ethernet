module bup_qsys_generic_tristate_controller_0 (
		input  wire        clk_clk,              //   clk.clk
		input  wire        reset_reset,          // reset.reset
		output wire        tcm_write_n_out,      //   tcm.write_n_out
		output wire        tcm_read_n_out,       //      .read_n_out
		output wire        tcm_chipselect_n_out, //      .chipselect_n_out
		output wire        tcm_request,          //      .request
		input  wire        tcm_grant,            //      .grant
		output wire [26:0] tcm_address_out,      //      .address_out
		output wire [15:0] tcm_data_out,         //      .data_out
		output wire        tcm_data_outen,       //      .data_outen
		input  wire [15:0] tcm_data_in,          //      .data_in
		input  wire [26:0] uas_address,          //   uas.address
		input  wire [1:0]  uas_burstcount,       //      .burstcount
		input  wire        uas_read,             //      .read
		input  wire        uas_write,            //      .write
		output wire        uas_waitrequest,      //      .waitrequest
		output wire        uas_readdatavalid,    //      .readdatavalid
		input  wire [1:0]  uas_byteenable,       //      .byteenable
		output wire [15:0] uas_readdata,         //      .readdata
		input  wire [15:0] uas_writedata,        //      .writedata
		input  wire        uas_lock,             //      .lock
		input  wire        uas_debugaccess       //      .debugaccess
	);
endmodule

