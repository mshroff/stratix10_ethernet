	bup_qsys_mm_bridge_0 u0 (
		.clk              (_connected_to_clk_),              //   clk.clk
		.m0_waitrequest   (_connected_to_m0_waitrequest_),   //    m0.waitrequest
		.m0_readdata      (_connected_to_m0_readdata_),      //      .readdata
		.m0_readdatavalid (_connected_to_m0_readdatavalid_), //      .readdatavalid
		.m0_burstcount    (_connected_to_m0_burstcount_),    //      .burstcount
		.m0_writedata     (_connected_to_m0_writedata_),     //      .writedata
		.m0_address       (_connected_to_m0_address_),       //      .address
		.m0_write         (_connected_to_m0_write_),         //      .write
		.m0_read          (_connected_to_m0_read_),          //      .read
		.m0_byteenable    (_connected_to_m0_byteenable_),    //      .byteenable
		.m0_debugaccess   (_connected_to_m0_debugaccess_),   //      .debugaccess
		.reset            (_connected_to_reset_),            // reset.reset
		.s0_waitrequest   (_connected_to_s0_waitrequest_),   //    s0.waitrequest
		.s0_readdata      (_connected_to_s0_readdata_),      //      .readdata
		.s0_readdatavalid (_connected_to_s0_readdatavalid_), //      .readdatavalid
		.s0_burstcount    (_connected_to_s0_burstcount_),    //      .burstcount
		.s0_writedata     (_connected_to_s0_writedata_),     //      .writedata
		.s0_address       (_connected_to_s0_address_),       //      .address
		.s0_write         (_connected_to_s0_write_),         //      .write
		.s0_read          (_connected_to_s0_read_),          //      .read
		.s0_byteenable    (_connected_to_s0_byteenable_),    //      .byteenable
		.s0_debugaccess   (_connected_to_s0_debugaccess_)    //      .debugaccess
	);

